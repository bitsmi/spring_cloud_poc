package com.bitsmi.spring.cloud.poc.configserver.event;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.context.event.ApplicationStartedEvent;
import org.springframework.context.ApplicationListener;

public class SpringBootStartedEventListener implements ApplicationListener<ApplicationStartedEvent>
{
	private final Logger log = LoggerFactory.getLogger(SpringBootStartedEventListener.class);
	
	@Override
	public void onApplicationEvent(ApplicationStartedEvent event) 
	{
		log.info("Spring Boot Application started");
	}
}
