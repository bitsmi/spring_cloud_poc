package com.bitsmi.spring.cloud.poc.configserver;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.config.server.EnableConfigServer;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.context.annotation.PropertySource;

import com.bitsmi.spring.cloud.poc.configserver.event.SpringBootFailedEventListener;
import com.bitsmi.spring.cloud.poc.configserver.event.SpringBootStartedEventListener;

//@SpringBootApplication == { @Configuration @EnableAutoConfiguration @ComponentScan }
@SpringBootApplication
@EnableConfigServer
@EnableEurekaClient
@PropertySource("file:conf/application.properties")
public class MainProgram 
{
	public static void main(String...args) throws Exception
	{
		SpringApplication application = new SpringApplication(MainProgram.class);
		application.addListeners(new SpringBootFailedEventListener(),
				new SpringBootStartedEventListener()
		);
		application.run(args);
	}
}
