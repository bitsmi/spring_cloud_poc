package com.bitsmi.spring.cloud.poc.configserver.event;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.context.event.ApplicationFailedEvent;
import org.springframework.context.ApplicationListener;

public class SpringBootFailedEventListener implements ApplicationListener<ApplicationFailedEvent> 
{
	private final Logger log = LoggerFactory.getLogger(SpringBootFailedEventListener.class);
	
	@Override
	public void onApplicationEvent(ApplicationFailedEvent event) 
	{
		log.info("Spring Boot Application failed to start");
	}
}
