package com.bitsmi.spring.cloud.poc.serviceregistry;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.server.EnableEurekaServer;
import org.springframework.context.annotation.PropertySource;

import com.bitsmi.spring.cloud.poc.serviceregistry.event.SpringBootFailedEventListener;
import com.bitsmi.spring.cloud.poc.serviceregistry.event.SpringBootStartedEventListener;

//@SpringBootApplication == { @Configuration @EnableAutoConfiguration @ComponentScan }
@SpringBootApplication
@EnableEurekaServer
@PropertySource("file:conf/application.properties")
public class MainProgram 
{
	public static void main(String...args) throws Exception
	{
		SpringApplication application = new SpringApplication(MainProgram.class);
		// Se especifica explicitamente que se utilice un AnnotationConfigEmbeddedWebApplicationContext
		application.setWebEnvironment(true);
		application.addListeners(new SpringBootFailedEventListener(),
				new SpringBootStartedEventListener()
		);
		application.run(args);
	}
}
