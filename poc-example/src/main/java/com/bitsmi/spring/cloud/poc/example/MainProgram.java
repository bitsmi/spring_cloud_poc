package com.bitsmi.spring.cloud.poc.example;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;

import com.bitsmi.spring.cloud.poc.example.event.SpringBootFailedEventListener;
import com.bitsmi.spring.cloud.poc.example.event.SpringBootStartedEventListener;

//@SpringBootApplication == { @Configuration @EnableAutoConfiguration @ComponentScan }
@SpringBootApplication
@EnableEurekaClient
@EnableDiscoveryClient
public class MainProgram 
{
	public static void main(String...args) throws Exception
	{
		SpringApplication application = new SpringApplication(MainProgram.class);
		// Se especifica explicitamente que se utilice un AnnotationConfigEmbeddedWebApplicationContext
		application.setWebEnvironment(true);
		application.addListeners(new SpringBootFailedEventListener(),
				new SpringBootStartedEventListener()
		);
		application.run(args);
	}
}
