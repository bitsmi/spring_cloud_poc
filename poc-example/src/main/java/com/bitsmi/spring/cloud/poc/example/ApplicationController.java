package com.bitsmi.spring.cloud.poc.example;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RefreshScope
public class ApplicationController 
{
	@Value("${workspacefolder}") 
	private String workspaceFolder;
	
	@RequestMapping("/")
	@ResponseBody
    public String home() 
	{
        return workspaceFolder;
    }
}
