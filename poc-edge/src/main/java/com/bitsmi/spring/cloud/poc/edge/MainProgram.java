package com.bitsmi.spring.cloud.poc.edge;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.zuul.EnableZuulProxy;
import com.bitsmi.spring.cloud.poc.edge.event.SpringBootFailedEventListener;
import com.bitsmi.spring.cloud.poc.edge.event.SpringBootStartedEventListener;

//@SpringBootApplication == { @Configuration @EnableAutoConfiguration @ComponentScan }
@SpringBootApplication
@EnableZuulProxy
public class MainProgram 
{
	public static void main(String...args) throws Exception
	{
		SpringApplication application = new SpringApplication(MainProgram.class);
		// Se especifica explicitamente que se utilice un AnnotationConfigEmbeddedWebApplicationContext
		application.setWebEnvironment(true);
		application.addListeners(new SpringBootFailedEventListener(),
				new SpringBootStartedEventListener()
		);
		application.run(args);
	}
}
